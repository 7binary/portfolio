<?php

namespace App\Bootstrap;

use Illuminate\Foundation\Application;

class SetServiceProvider
{
    /**
     * Register custom ServiceProvider during bootstrap.
     */
    private $providers = [
        \Asvae\ApiTester\ServiceProvider::class,
    ];

    public function bootstrap(Application $app)
    {
        if ($app->environment('local')) {
            foreach ($this->providers as $provider) {
                $app['config']->push('app.providers', $provider);
            }
        }
    }
}