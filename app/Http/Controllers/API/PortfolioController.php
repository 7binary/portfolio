<?php

namespace App\Http\Controllers\API;

use App\Models\PortfolioBlock;
use App\Http\Controllers\Controller;
use App\Models\PortfolioCategory;
use App\Models\PortfolioProject;

class PortfolioController extends Controller
{
    public function blocks()
    {
        $blocksRaw = PortfolioBlock::all()->sortBy('id')->toArray();
        $categoriesRaw = PortfolioCategory::all()->sortBy('id')->toArray();
        $projectsRaw = PortfolioProject::all()->sortBy('id')->toArray();

        $categories = [];
        if (!empty($categoriesRaw)) {
            foreach ($categoriesRaw as $category) {
                $key = $category['id'] . '';
                $category['projects'] = [];
                $categories[$key] = $category;
            }
        }

        if (!empty($projectsRaw)) {
            foreach ($projectsRaw as $project) {
                $key = $project['category_id'] . '';
                if (isset($categories[$key])) {
                    $categories[$key]['projects'][] = $project;
                }
            }
        }

        $blocks = [];
        if (!empty($blocksRaw)) {
            foreach ($blocksRaw as $block) {
                $key = $block['id'];
                $block['categories'] = [];
                $blocks[$key] = $block;
            }
        }

        if (!empty($categories)) {
            foreach ($categories as $categoryId => $category) {
                $key = $category['block_id'];
                if (isset($blocks[$key])) {
                    $blocks[$key]['categories'][] = $category;
                }
            }
        }

        return array_values($blocks);
    }
}
