<?php

namespace App\Http\Controllers\Admin;

use App\Models\PortfolioCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PortfolioProjectRequest as StoreRequest;
use App\Http\Requests\PortfolioProjectRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class PortfolioProjectCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PortfolioProjectCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\PortfolioProject');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/portfolioproject');
        $this->crud->setEntityNameStrings('portfolioproject', 'portfolio_projects');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addField([
            'label' => 'Категория',
            'type' => 'select2',
            'name' => 'category_id', // the db column for the foreign key
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'ru_title', // foreign key attribute that is shown to user
            'model' => PortfolioCategory::class, // foreign key model
        ]);

        $this->crud->addField([ // image
            'label' => "Логотип",
            'name' => "logo",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            'prefix' => 'storage/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([ // image
            'label' => "Фоновое изображение",
            'name' => "image",
            'type' => 'image',
            'upload' => true,
            //'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            'prefix' => 'storage/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([
            'type' => 'tinymce',
            'name' => 'ru_description',
        ]);

        $this->crud->addField([
            'type' => 'tinymce',
            'name' => 'en_description',
        ]);

        // add asterisk for fields that are required in PortfolioProjectRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
