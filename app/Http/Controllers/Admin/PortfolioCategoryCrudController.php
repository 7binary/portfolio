<?php

namespace App\Http\Controllers\Admin;

use App\Models\PortfolioBlock;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PortfolioCategoryRequest as StoreRequest;
use App\Http\Requests\PortfolioCategoryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class PortfolioCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PortfolioCategoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\PortfolioCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/portfoliocategory');
        $this->crud->setEntityNameStrings('portfoliocategory', 'portfolio_categories');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addField([
            'label' => 'Блок',
            'type' => 'select2',
            'name' => 'block_id', // the db column for the foreign key
            'entity' => 'block', // the method that defines the relationship in your Model
            'attribute' => 'ru_title', // foreign key attribute that is shown to user
            'model' => PortfolioBlock::class, // foreign key model
        ]);

        $this->crud->addField([ // image
            'label' => "Фоновое изображение",
            'name' => "image",
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            // 'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            'prefix' => 'storage/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([
            'type' => 'tinymce',
            'name' => 'ru_description',
        ]);

        $this->crud->addField([
            'type' => 'tinymce',
            'name' => 'en_description',
        ]);

        $this->crud->addField([
            'label' => "Мобильные приложения",
            'type' => 'checkbox',
            'name' => 'is_mobile',
        ]);

        $this->crud->addColumn([
            'name' => 'is_mobile',
            'label' => "Мобильные приложения",
            'type' => 'check'
        ]);

        // add asterisk for fields that are required in PortfolioCategoryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
