<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class SetApiHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        $response->withHeaders([
            'Content-Type' => 'application/json',
            'Allow' => 'GET, POST',
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'Origin, Content-Type, X-Auth-Token, X-Requested-With, Authorization, Accept',
        ]);

        return $response;
    }
}