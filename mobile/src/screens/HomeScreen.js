import React from 'react';
import { Text, View, TouchableOpacity, ImageBackground, StyleSheet, BackHandler, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { compose } from '../utils';
import withProjectsService from '../services/withProjectsService';
import { fetchProjects } from '../actions';

const Block = (props) => {
  const { navigation } = props;
  const { en_title, en_description, color, url, image_url } = props.block;
  const screenName = url.substr(0, 1).toUpperCase() + url.substr(1).toLowerCase();
  return (
    <TouchableOpacity style={{ ...styles.containerCenter, ...styles.block }} activeOpacity={.8}
                      onPress={() => navigation.navigate(screenName)}>
      <ImageBackground source={{ uri: image_url }} style={styles.backgroundImage}/>
      <Text style={{ ...styles.title, color }}>{en_title}</Text>
      <Text style={styles.description}>{en_description}</Text>
    </TouchableOpacity>
  );
};

const BlockList = (props) => {
  const { blocks, navigation } = props;
  return blocks.map((block) => <Block key={block.id} block={block} navigation={navigation}/>);
};

class HomeScreen extends React.Component {
  componentDidMount() {
    this.props.fetchProjects();

    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('Start');
      return true;
    });

    this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      StatusBar.setBackgroundColor('#222');
    });
  }

  render() {
    const { blocks, navigation } = this.props;
    if (!blocks || blocks.length === 0) {
      return null;
    }

    return (
      <View style={styles.containerCenter}>
        <BlockList blocks={blocks} navigation={navigation}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  block: {
    width: '100%',
    backgroundColor: 'black',
    paddingBottom: 2,
  },
  title: {
    padding: 3,
    fontSize: 19,
    fontWeight: 'bold',
  },
  description: {
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 17,
    fontStyle: 'italic',
    color: 'white',
    textAlign: 'center',
  },
  backgroundImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
    opacity: .4,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
  },
  backgroundLayout: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
  },
});

const mapStateToProps = ({ projects: { blocks, loading, error } }) => {
  return { blocks, loading, error };
};

const mapDispatchToProps = (dispatch, { projectsService }) => {
  return bindActionCreators({
    fetchProjects: fetchProjects(projectsService),
  }, dispatch);
};

export default compose(
  withProjectsService,
  connect(mapStateToProps, mapDispatchToProps),
)(HomeScreen);
