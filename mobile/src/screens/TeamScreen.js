import React from 'react';
import Block from '../components/Block';
import { StatusBar } from 'react-native';

export default class TeamScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'TeamLead',
  };

  componentDidMount() {
    this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('white');
    });
  }

  render() {
    return <Block url='team'/>;
  }
}
