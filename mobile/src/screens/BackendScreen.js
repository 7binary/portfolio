import React from 'react';
import Block from '../components/Block';
import { StatusBar } from 'react-native';

export default class AdminScreen extends React.Component {
  componentDidMount() {
    this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('white');
    });
  }

  render() {
    return <Block url='backend'/>;
  }
}
