import React from 'react';
import {createDrawerNavigator} from 'react-navigation-drawer';

import TabsNavigator from "./TabsNavigator";
import HomeScreen from "../screens/HomeScreen";

const AppNavigator = createDrawerNavigator(
  {
    Start: HomeScreen,
    Tabs: TabsNavigator,
  }, {
    drawerWidth: 0,
  },
);

export default AppNavigator;
