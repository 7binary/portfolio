import {createBottomTabNavigator} from 'react-navigation-tabs';
import BackendScreen from '../screens/BackendScreen';
import FrontendScreen from '../screens/FrontendScreen';
import MobileScreen from '../screens/MobileScreen';
import ArchitectScreen from '../screens/ArchitectScreen';
import AdminScreen from '../screens/AdminScreen';
import TeamScreen from '../screens/TeamScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';

const TabsNavigator = createBottomTabNavigator(
  {
    Backend: BackendScreen,
    Frontend: FrontendScreen,
    Mobile: MobileScreen,
    Architect: ArchitectScreen,
    Admin: AdminScreen,
    Team: TeamScreen,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName = null;
        switch (routeName) {
          case 'Backend':
            iconName = `ios-code`;
            break;
          case 'Frontend':
            iconName = `ios-desktop`;
            break;
          case 'Mobile':
            iconName = `ios-phone-portrait`;
            break;
          case 'Architect':
            iconName = `ios-cube`;
            break;
          case 'Admin':
            iconName = `ios-settings`;
            break;
          case 'Team':
            iconName = `ios-people`;
            break;
        }

        return <Icon name={iconName} size={25} color={tintColor}/>;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#2575b6',
      inactiveTintColor: 'gray',
    },
  },
);

export default TabsNavigator;
