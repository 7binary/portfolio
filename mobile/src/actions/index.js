import {
  FETCH_PROJECTS_REQUEST,
  FETCH_PROJECTS_SUCCESS,
  FETCH_PROJECTS_FAILURE,
} from '../actionTypes';

const projectsRequested = () => {
  return {
    type: FETCH_PROJECTS_REQUEST,
  };
};

const projectsLoaded = (projects) => {
  return {
    type: FETCH_PROJECTS_SUCCESS,
    payload: projects,
  };
};

const projectsError = (error) => {
  return {
    type: FETCH_PROJECTS_FAILURE,
    payload: error,
  };
};

const fetchProjects = (projectsService) => () => (dispatch) => {
  dispatch(projectsRequested());
  projectsService.getProjects()
    .then((response) => dispatch(projectsLoaded(response.data)))
    .catch((err) => dispatch(projectsError(err)));
};

export {
  fetchProjects,
};
