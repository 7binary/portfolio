import React from 'react';

const {
  Provider: ProjectsServiceProvider,
  Consumer: ProjectsServiceConsumer,
} = React.createContext();

export {
  ProjectsServiceProvider,
  ProjectsServiceConsumer,
};