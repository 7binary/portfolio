import React from 'react';
import { ProjectsServiceConsumer } from './ProjectsServiceContext';

const withProjectsService = (Wrapped) => {
  return (props) => {
    return (
      <ProjectsServiceConsumer>
        {
          (projectsService) => {
            return <Wrapped {...props} projectsService={projectsService}/>;
          }
        }
      </ProjectsServiceConsumer>
    );
  };
};

export default withProjectsService;