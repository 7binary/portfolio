import ax from '../utils/ax';

export default class ProjectsService {
  getProjects = () => {
    return ax.get('api/blocks');
  };
}