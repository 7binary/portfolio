import React from 'react';
import Block from '../components/Block';

const createBlock = (url) => () => {
  return <Block url={url}/>
};

export default createBlock;
