import {
  FETCH_PROJECTS_REQUEST,
  FETCH_PROJECTS_SUCCESS,
  FETCH_PROJECTS_FAILURE,
} from '../actionTypes';

const updateProjects = (state, action) => {
  if (state === undefined) {
    return {
      blocks: [],
      loading: false,
      error: null,
    };
  }

  switch (action.type) {
    case FETCH_PROJECTS_REQUEST:
      return {
        blocks: [],
        loading: true,
        error: null,
      };
    case FETCH_PROJECTS_SUCCESS:
      return {
        blocks: action.payload,
        loading: false,
        error: null,
      };
    case FETCH_PROJECTS_FAILURE:
      return {
        blocks: [],
        loading: false,
        error: action.payload,
      };
  }

  return state;
};

export default updateProjects;