import updateProjects from './projects';
import updateUser from './user';

const rootReducer = (state, action) => {
  return {
    projects: updateProjects(state, action),
    user: updateUser(state, action),
  };
};

export default rootReducer;