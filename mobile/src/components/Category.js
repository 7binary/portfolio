import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import HTMLView from 'react-native-htmlview';

import ProjectList from './ProjectList';

const Category = (props) => {
  const { en_title, en_description, image_url, projects } = props.category;
  return (
    <View style={styles.category}>
      <View style={styles.imageView}>
        <Image source={{ uri: image_url }} style={styles.image}/>
      </View>
      <Text style={styles.title}>{en_title}</Text>
      <View style={styles.description}>
        <HTMLView value={en_description}/>
      </View>
      <ProjectList projects={projects}/>
    </View>
  );
};

const styles = StyleSheet.create({
  category: {
    textAlign: 'center',
    paddingBottom: 20,
  },
  imageView: {
    height: 100,
    overflow: 'hidden',
    backgroundColor: 'white',
    borderBottomWidth: 2,
    borderBottomColor: '#eee',
  },
  image: {
    height: 120,
  },
  title: {
    paddingTop: 15,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    textShadowColor: 'gray',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 5,
  },
  description: {
    paddingLeft: 15,
    paddingRight: 15,
  },
});

export default Category;