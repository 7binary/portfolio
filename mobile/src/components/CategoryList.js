import React from 'react';
import { ScrollView } from 'react-native';

import Category from './Category';

const CategoryList = (props) => {
  const { categories } = props;
  return (
    <ScrollView>
      {categories.map((category) => <Category key={category.id} category={category}/>)}
    </ScrollView>
  );
};

export default CategoryList;