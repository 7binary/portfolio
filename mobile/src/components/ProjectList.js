import React from 'react';
import { View } from 'react-native';

import Project from './Project';

const ProjectList = (props) => {
  const { projects } = props;
  if (!projects || !projects.length) {
    return null;
  }
  return (
    <View style={{}}>
      {projects.map((project) => <Project key={project.id} project={project}/>)}
    </View>
  );
};

export default ProjectList;