/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { StatusBar } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { PersistGate } from 'redux-persist/integration/react'

import configureStore from '../configureStore';
import AppNavigator from '../navigation/AppNavigator';
import ProjectsService from '../services/ProjectsService';
import { ProjectsServiceProvider } from '../services/ProjectsServiceContext';

const AppContainer = createAppContainer(AppNavigator);
const { store, persistor } = configureStore();

store.subscribe(() => {
  const { projects: { blocks } } = store.getState();
  if (blocks && blocks.length > 0) {
    SplashScreen.hide();
  }
});

export default class App extends Component {
  state = {
    projectsService: new ProjectsService(),
  };

  componentDidMount() {
    setTimeout(() => SplashScreen.hide(), 2500);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ProjectsServiceProvider value={this.state.projectsService}>
            <StatusBar backgroundColor="#222" barStyle="light-content"/>
            <AppContainer/>
          </ProjectsServiceProvider>
        </PersistGate>
      </Provider>
    );
  }
};