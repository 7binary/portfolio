import React from 'react';
import { connect } from 'react-redux';
import CategoryList from './CategoryList';

class Block extends React.Component {
  render() {
    const { blocks, url } = this.props;
    const block = blocks.find((el) => el.url === url);
    if (!block) {
      return null;
    }
    const { categories } = block;
    return <CategoryList categories={categories}/>;
  }
}

const mapStateToProps = ({ projects: { blocks } }) => {
  return {
    blocks,
  };
};

export default connect(mapStateToProps)(Block);
