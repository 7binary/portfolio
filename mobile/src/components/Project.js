import React from 'react';
import { Image, Linking, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const Project = (props) => {
  const { image_url, logo_url, website, en_title } = props.project;
  const loadInBrowser = (url) => {
    Linking.openURL(url).catch(err => console.error(`Couldn't load page ${url}`, err));
  };
  return (
    <View style={styles.project}>
      <TouchableOpacity onPress={() => loadInBrowser(website)}>
        <Text style={styles.title}>{en_title}</Text>
        <View style={styles.images}>
          <View style={styles.imageView}>
            <Image source={{ uri: logo_url }} resizeMode={'cover'} style={styles.image}/>
          </View>
          <View style={styles.imageView}>
            <Image source={{ uri: image_url }} resizeMode={'cover'} style={styles.image}/>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  project: {
    flex: 1,
    marginBottom: 10,
  },
  title: {
    padding: 5,
    fontSize: 18,
    color: 'steelblue',
    fontWeight: 'bold',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  images: {
    flexDirection: 'row',
    paddingLeft: 4,
    paddingRight: 4,
  },
  imageView: {
    flex: .5,
    padding: 4,
  },
  image: {
    width: '100%',
    height: 150,
    borderColor: '#eee',
    borderWidth: 1,
  },
});

export default Project;