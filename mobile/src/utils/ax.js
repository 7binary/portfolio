import axios from 'axios';
//   axios.defaults.headers.common['Authorization'] = token ? `Bearer ${token}` : null;

const baseURL = 'https://api.zin.is';

const ax = axios.create({
  baseURL,
  timeout: 60000,
  headers: { 'Content-Type': 'application/json' },
});

export default ax;
