<?php
namespace Deployer;

/**
 * Deploying recipe for deployer utility
 *
 * NOTE: you should add the following permission to the sudo file (with visudo command)
 *
 * user_name ALL=(ALL) NOPASSWD: /usr/bin/setfacl
 */

require 'recipe/common.php';

inventory('deploy/servers.yml');

set('ssh_type', 'native');
set('ssh_multiplexing', false);
set('keep_releases', 2);
set('default_timeout', null);

// Do not use sudo to setup writable folders.
// You should setup access to setfacl via sudoers file on the server
set('writable_use_sudo', false);

set('repository', 'git@bitbucket.org:7binary/portfolio.git');

// Laravel shared dirs
set('shared_dirs', [
    'storage',
]);
// Laravel shared file
set('shared_files', [
    '.env',
]);
// Laravel writable dirs
set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);

task('artisan:up', function () {
    $output = run('if [ -f {{deploy_path}}/current/artisan ]; then {{bin/php}} {{deploy_path}}/current/artisan up; fi');
    writeln('<info>' . $output . '</info>');
})->desc('Disable maintenance mode');

task('artisan:down', function () {
    $output = run('if [ -f {{deploy_path}}/current/artisan ]; then {{bin/php}} {{deploy_path}}/current/artisan down; fi');
    writeln('<info>' . $output . '</info>');
})->desc('Enable maintenance mode');

task('artisan:migrate', function () {
    run('{{bin/php}} {{release_path}}/artisan migrate --force');
})->desc('Execute artisan migrate');

task('artisan:migrate:status', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan migrate:status');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan migrate:status');

task('artisan:db:seed', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan db:seed --force');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan db:seed');

task('artisan:cache:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan cache:clear');
})->desc('Execute artisan cache:clear');

task('artisan:config:cache', function () {
    run('{{bin/php}} {{release_path}}/artisan config:cache');
})->desc('Execute artisan config:cache');

task('artisan:route:cache', function () {
    run('{{bin/php}} {{release_path}}/artisan route:cache');
})->desc('Execute artisan route:cache');

task('artisan:view:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan view:clear');
})->desc('Execute artisan view:clear');

task('artisan:storage:link', function () {
    run('{{bin/php}} {{release_path}}/artisan storage:link');
})->desc('Execute artisan storage:link');

task('reload:php-fpm', function () {
    run('sudo /usr/sbin/service php7.3-fpm reload');
})->desc('Reload PHP-FPM service');

task('prepare', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:copy_dirs',
    'deploy:vendors',
    'deploy:symlink',
    'cleanup'
])->desc('Prepare library, using first time deploy');

task('deploy', [
    'deploy:prepare',
    // 'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:clear',
    'artisan:config:cache',
    'artisan:route:cache',
    'artisan:migrate',
    'artisan:migrate:status',
    'deploy:symlink',
    // 'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');

after('deploy', 'reload:php-fpm');
after('deploy', 'success');
