<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkPortfolioCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolio_categories', function (Blueprint $table) {
            $table->foreign('block_id')
                ->references('id')->on('portfolio_blocks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolio_categories', function (Blueprint $table) {
            $table->dropForeign('portfolio_categories_block_id_foreign');
        });
    }
}
