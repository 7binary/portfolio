<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->nullable();
            $table->string('color')->nullable();
            $table->string('ru_title')->nullable();
            $table->string('ru_description', 1000)->nullable();
            $table->string('en_title')->nullable();
            $table->string('en_description', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_blocks');
    }
}
