<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href='{{ backpack_url('portfolioblock') }}'><i class='fa fa-tag'></i> <span>Блоки проектов</span></a></li>
<li><a href='{{ backpack_url('portfoliocategory') }}'><i class='fa fa-tag'></i> <span>Категории проектов</span></a></li>
<li><a href='{{ backpack_url('portfolioproject') }}'><i class='fa fa-tag'></i> <span>Проекты</span></a></li>