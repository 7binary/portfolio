import Vue from 'vue';
import Router from 'vue-router';

import Index from './pages/Index.vue';
import Block from './pages/Block.vue';
import NotFound from './pages/NotFound.vue';

Vue.use(Router);

const routes = [
  { path: '/', component: Index },
  { path: '/block/:blockUrl', component: Block, props: true, meta: { routeName: 'block' } },
  { path: '*', component: NotFound },
];

const router = new Router({
  mode: 'history',
  routes,
});

export default router;
