import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import axios from 'axios';

Vue.use(Vuex);

const baseURL = process.env.NODE_ENV === 'production' ? 'https://api.zin.is' : 'http://portfolio.f';

const store = new Vuex.Store({
  plugins: [createPersistedState({ key: 'spa', ssr: false })],
  state: {
    loading: false,
    blocks: [],
    lang: 'en',
    blockUrl: null,
  },
  mutations: {
    SetLoading(state, payload) {
      state.loading = payload;
    },
    SetBlocks(state, payload) {
      state.blocks = payload;
    },
    SetLang(state, payload) {
      state.lang = payload;
    },
    SetBlockUrl(state, payload) {
      state.blockUrl = payload;
    },
  },
  actions: {
    SetBlockUrl({ commit }, url) {
      commit('SetBlockUrl', url);
    },
    SetLang({ commit }, lang) {
      commit('SetLang', lang);
    },
    SetLoading({ commit }, loading) {
      commit('SetLoading', loading);
    },
    LoadBlocks({ getters, commit }) {
      getters.ax.get('api/blocks')
        .then((response) => {
          commit('SetBlocks', response.data);
        });
    },
    UnsetError() {
      Vue.notify({ group: 'error', clean: true });
    },
    UnsetInfo() {
      Vue.notify({ group: 'info', clean: true });
    },
    ShowInfo({ dispatch }, message) {
      dispatch('UnsetError');
      dispatch('UnsetInfo');
      dispatch('ShowInfoMessage', message);
    },
    ShowError({ dispatch }, message) {
      dispatch('UnsetError');
      dispatch('UnsetInfo');
      dispatch('ShowErrorMessage', message);
    },
    ShowErrorMessage({}, message) {
      const text = `<i class="v-icon v-icon--left material-icons mr-1" style="vertical-align:bottom">error_outline</i> ${message}`;
      Vue.notify({ group: 'error', type: 'error', text, duration: 60000 });
    },
    ShowInfoMessage({}, message) {
      const text = `<i class="v-icon v-icon--left material-icons mr-1" style="vertical-align:bottom">notifications</i> ${message}`;
      Vue.notify({ group: 'info', type: 'info', text, duration: 7000 });
    },
    HandleError({ dispatch }, error) {
      if (error.response) {
        // Error recieved from the server
        const { data } = error.response;
        let errors = [];
        if ('error' in data) {
          errors = [data.error];
        } else if ('errors' in data) {
          if (Array.isArray(data.errors)) {
            ({ errors } = data);
          } else {
            Object.keys(data.errors).forEach((k) => {
              errors.push(data.errors[k]);
            });
          }
        }
        // maximum show 2 errors
        if (errors.length > 2) {
          errors = errors.slice(0, 2);
        }
        errors.forEach((message) => {
          dispatch('ShowErrorMessage', message);
        });
      } else if (error.request) {
        // The request was made but no response was received
        dispatch('ShowErrorMessage', 'С вашим подключением не все так просто. Пожалуйста, проверьте доступ к интернету');
      } else {
        // Something happened in setting up the request that triggered an Error
        dispatch('ShowErrorMessage', 'Что-то пошло не так');
        console.log(error.config);
      }
    },
  },
  getters: {
    ax() {
      return axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json' },
      });
    },
    axl(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json' },
      });
      ax.interceptors.request.use(
        (config) => {
          state.loading = true;
          return config;
        },
      );
      ax.interceptors.response.use(
        (response) => {
          state.loading = false;
          Vue.notify({ group: 'error', clean: true });
          return response;
        },
        (error) => {
          state.loading = false;
          Vue.notify({ group: 'error', clean: true });
          return Promise.reject(error);
        },
      );
      return ax;
    },
  },
});

export default store;
