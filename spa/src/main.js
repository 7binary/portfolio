import velocity from 'velocity-animate';
import Notifications from 'vue-notification';
import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
import Vuetify from 'vuetify';
import Tooltip from 'vue-directive-tooltip';
import router from './routes';
import 'vuetify/dist/vuetify.min.css';
import './assets/sass/main.scss';
import store from './store/storage';
import App from './App.vue';

// import CSS
require('vue-directive-tooltip/css/index.css');

const isProd = process.env.NODE_ENV === 'production';

// Vue plugins
Vue.use(Vuetify);
Vue.use(Tooltip);
Vue.use(Notifications, { velocity });
Vue.config.productionTip = false;
Vue.use(VueAnalytics, {
  id: 'UA-135965238-1',
  router,
  autoTracking: {
    exception: true,
  },
  debug: {
    enabled: !isProd,
    sentHitTask: isProd,
  },
});

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.commit('SetLoading', false);
    this.$store.dispatch('LoadBlocks', false);
  },
}).$mount('#app');
